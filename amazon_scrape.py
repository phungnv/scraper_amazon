#!/usr/bin/env python

import re
import time
import csv
import random
import traceback
import subprocess
from subprocess import call, check_call, CalledProcessError
from os import devnull
from time import sleep
from sys import stdout, stderr

from lxml import html
from multiprocessing.dummy import Pool
from selenium import webdriver
from selenium.webdriver import ChromeOptions

start_time = time.time()

PROXY_TOR = 'socks5://127.0.0.1:9050'
# Constants. You can edit code only in this part.
MAIN_LINKS = {#'Computers and tablets':'https://www.amazon.com/s?i=computers&bbn=541966&rh=n%3A172282%2Cn%3A493964%2Cn%3A541966%2Cn%3A13896617011%2Cp_n_shipping_option-bin%3A3242350011&dc&page={}',
              #'Headphones':'https://www.amazon.com/s?k=headphones&i=electronics&rh=n%3A172541%2Cp_72%3A1248879011&dc&page={}',
              'cat_1': 'https://www.amazon.com/s?i=baby-products-intl-ship&rh=n%3A%2116225005011&page={}',
              'cat_2': 'https://www.amazon.com/s?i=kitchen-intl-ship&rh=n%3A%2116225011011&page={}',
              'cat_3': 'https://www.amazon.com/s?i=hpc-intl-ship&rh=n%3A%2116225010011&page={}',
              'cat_4': 'https://www.amazon.com/s?i=beauty-intl-ship&rh=n%3A%2116225006011&page={}',
              'cat_5': 'https://www.amazon.com/s?i=electronics-intl-ship&rh=n%3A%2116225009011&page={}',
              'cat_7': 'https://www.amazon.com/s?rh=n%3A16225007011&page={}',
              'cat_8': 'https://www.amazon.com/s?rh=n%3A2562090011&page={}',
              'cat_9': 'https://www.amazon.com/s?i=pets-intl-ship&rh=n%3A%2116225013011&page={}',
              'cat_10': 'https://www.amazon.com/s?i=toys-and-games-intl-ship&rh=n%3A%2116225015011&page={}',
              'cat_11': 'https://www.amazon.com/s?i=videogames-intl-ship&rh=n%3A%2116225016011&page={}',
              'cat_12': 'https://www.amazon.com/s?i=sporting-intl-ship&rh=n%3A%2116225014011&page={}',
              'cat_13': 'https://www.amazon.com/s?rh=n%3A256643011&page={}',
              }
WRITE_DB = 'amazon_DB.csv' # A database, where we write data scraped.
DB_HEADER = ['title','brand','colors','sku','rating','platform','size',
             'picture', 'price', 'category', 'condition','url'] # Fields, that we scrape.
HEADER_DB = 'headers.txt'
# Number of threads. How many browsers open at the same time. The more, the harder it for a computer, but scraper will work faster.
THREADS = 2
NEED_SCRAPE = 60000# The number of products we need to scrape. If it's 1000, we scrape around 250 for each category.


# This is a Chrome WebDriver class. You don't need to edit this block. It works well.
class WebDriver():
    def __init__(self):
        self.options = ChromeOptions()
        self.options.add_argument('--window-size=1366,768')
        self.options.add_argument('--user-agent={0}'.format(random.choice(headers_list)))
        self.options.add_argument('--headless')
        # experimental options
        self. blink_settings_list = []
        self.disabled_features_list = []
        self.chrome_prefs = {}

        self.chrome_prefs['profile.default_content_setting_values.notifications'] = 2
        self.chrome_prefs['profile.default_content_setting_values.images'] = 2
        self.blink_settings_list.append('imagesEnabled=false')
        self.blink_settings_list.append('loadsImagesAutomatically=false')
        self.blink_settings_list.append('mediaPlaybackRequiresUserGesture=true')
        self.disabled_features_list.append('PreloadMediaEngagementData')
        self.disabled_features_list.append('AutoplayIgnoreWebAudio')
        self.disabled_features_list.append('MediaEngagementBypassAutoplayPolicies')
        if self.disabled_features_list:
            self.options.add_argument(f"--disable-features={','.join(self.disabled_features_list)}")
        if self.blink_settings_list:
            self.options.add_argument(f"--blink-settings={','.join(self.blink_settings_list)}")
        if self.chrome_prefs:
            self.options.add_experimental_option('prefs', self.chrome_prefs)
        self.options.add_argument('--disable-device-discovery-notifications')
        self.options.add_argument('--disable-dev-shm-usage')
        self.options.add_argument('--no-sandbox')
        self.options.add_argument('--proxy-server=' + PROXY_TOR)
        #
        self.options.add_argument('--allow-insecure-localhost')
        #experiment end.
        self.mydriver = webdriver.Chrome(options=self.options)

    # Get html-object of current page in browser.
    def page(self):
        page_text = self.mydriver.page_source
        return html.fromstring(page_text)

    # Find element by XPath.
    def xpath(self, path):
        return self.mydriver.find_element_by_xpath(path)

    # Find a list of elements by Xpath.
    def xpathes(self, path):
        return self.mydriver.find_elements_by_xpath(path)

    # Go to the page.
    def get(self, url):
        self.mydriver.get(url)

    # Get source code of a page as str value.
    def get_source(self):
        return self.mydriver.page_source

    # Close the browser.
    def quit(self):
        self.mydriver.quit()


# Function for a comfortable logging in a command line.
def message(text):
    print(time.strftime('%X: '), text, flush=True)


# Returns a substring between 2 elements in a string.
def between_markers(text: str, begin: str, end: str) -> str:
    start = text.find(begin) + len(begin) if begin in text else None
    stop = text[start:].find(end) if end in text else None
    return text[start:start+stop]


# Special function that finds and returns Item model number of a product.
def get_model_number(source_page):
    dom2 = html.fromstring(source_page)
    if dom2.xpath('.//b[contains(.,"Item model")]/text()'):
        return dom2.xpath('.//li[contains(.,"Item model")]/text()')[0].strip()
    begin = 'Item model number'
    end = '</td>'
    start = source_page.find(begin) + len(begin) if begin in source_page else None
    stop = source_page[start:].find(end) if end in source_page else None
    model_num = source_page[start:start+stop]
    model_num = model_num.strip()
    start = model_num.rfind('>')
    model_num = model_num[start+1:].strip()
    if len(model_num)==0:
        model_num = dom2.xpath('.//tr[contains(.,"Item model number")]/td[2]/text()')
        model_num = ''.join(model_num)
        model_num = model_num.strip()
    return model_num


def tor():
    fnull = open(devnull, 'w')
    try:
        tor_restart = check_call( ["sudo", "service", "tor", "restart"], stdout=fnull, stderr=fnull)
        time.sleep(5)

        if tor_restart is 0:
            print(" {0}".format( "[\033[92m+\033[0m] Anonymizer status \033[92m[ON]\033[0m"))
            print(" {0}".format( "[\033[92m*\033[0m] Getting public IP, please wait..."))
            retries = 0
            my_public_ip = None
            while retries < 12 and not my_public_ip:
                retries += 1
    except CalledProcessError as err:
        print("\033[91m[!] Command failed: %s\033[0m" % ' '.join(err.cmd))

# Worker - this is a browser. We create as many workers, as many THREADS we use. One worker is a browser.
def worker(categ):
    global with_model_num, total_counter, curr_page
    message('Chosen category: {0}'.format(categ))
    page_counter = 1

    while True:
        message('For "{0}" process page № {1}'.format(categ, page_counter))
        search_page = MAIN_LINKS[categ].format(page_counter)
        message('The link: {0}'.format(search_page))
        mydriver = WebDriver()
        mydriver.get(search_page)
        page = mydriver.get_source()
        if 'Sorry, we just need to make sure you' in page:
            message('Amazon has blocked our IP. We are waiting')
            tor()
            continue
        dom = html.fromstring(page)
        curr_links = dom.xpath(
            './/h2[@class="a-size-mini a-spacing-none a-color-base s-line-clamp-2"]/a[@class="a-link-normal a-text-normal"]/@href')
        curr_links = [url[:url.find('ref')] for url in curr_links]
        if len(curr_links) == 0:
            curr_links = dom.xpath('.//a[@class="a-link-normal s-access-detail-page s-color-twister-title-link a-text-normal"]/@href')
            if curr_links:
                curr_links = [link[link.rfind("amazon.com")+ len("amazon.com"):] for link in curr_links]
                curr_links = [url[:url.find('ref')] for url in curr_links]
            else:
                curr_links = dom.xpath('.//a[@class="a-link-normal a-text-normal"]/@href')
                curr_links = [url[:url.find('ref')] for url in curr_links]
                if not curr_links:
                    message('Amazon has blocked our IP. We are waiting')
                    tor()
                    continue
        else:
            print(3)
            curr_links = [url[:url.find('ref')] for url in curr_links]

        ##################
        for pre_link in curr_links:
            special = False
            if with_model_num >= NEED_SCRAPE:
                mydriver.quit()
                return 0
            try:
                print('-'*50)

                platform = 'Amazon'
                curr_link = 'https://www.amazon.com' + pre_link

                message('Processing this product: {0}'.format(curr_link))
                mydriver.get(curr_link)
                loc_page = mydriver.get_source()
                loc_dom = html.fromstring(loc_page)
                try:
                    title = loc_dom.xpath('.//span[@id="productTitle"]/text()')[0].strip()
                except:
                    message('Amazon has blocked our IP. We are changing it')
                    tor()
                    continue

                # Check if a product has Item model number. We only need those who has.
                if 'Item model' in loc_page:
                    model_num = get_model_number(loc_page)
                    if len(model_num) < 5:
                        message('This item model is too short. It must be a mistake.')
                        continue
                else:
                    message('There is no Item model number for the product. Skip it.')
                    continue

                # Scrape other fields we need
                message('Item model number: {0}'.format(model_num))
                message('Title : {0}'.format(title))
                
                brand = loc_dom.xpath('.//a[@id="bylineInfo"]/text()')[0].strip()
                message('Brand : {0}'.format(brand))

                category = loc_dom.xpath("//div[@id='wayfinding-breadcrumbs_feature_div']/ul/li[not(@class)]/span/a/text()")
                category = ' > '.join([i.strip() for i in category]) if category else None
                message('Category : {0}'.format(category))

                colors = loc_dom.xpath('.//div[@id="variation_color_name"]//li[contains(@title,"Click to select")]/@title')
                if bool(colors) is True:
                    colors = [i[16:] for i in colors]
                    colors = ', '.join(colors)
                else:
                    colors = 'Only default color available'
                message('Colors : {0}'.format(colors))

                #################
                sizes = loc_dom.xpath('.//div[@id="variation_size_name"]//li[contains(@title,"Click to select")]/@title')
                if bool(sizes) is True:
                    sizes = [i[16:] for i in sizes]
                    sizes = ', '.join(sizes)
                else:
                    sizes = 'Only default size available'
                message('Sizes : {0}'.format(sizes))
                ##############
                rating = loc_dom.xpath('.//span[@id="acrPopover"]/@title')
                if rating:
                    rating = rating[0][:3]
                else:
                    rating = 'No rating for this product yet'
                message('Rating : {0}'.format(rating))
                message('Platform: {0}'.format(platform))
                image = loc_dom.xpath('.//img[@id="landingImage"]/@data-old-hires')
                if image:
                    image = image[0]
                message('Picture URL: {0}'.format(image))
                # Trying to scrape price in this part. It's a difficult task.
                price = loc_dom.xpath('.//span[@id="priceblock_ourprice"]/text()')
                if price:
                    #message('PRICE: 1')
                    price = price[0]
                else:
                    price = loc_dom.xpath('.//td[@class="comparison_baseitem_column"]//span[@class="a-offscreen"]/text()')
                    if price:
                       # message('PRICE: 2')
                        price = price[0]
                    else:
                        if loc_dom.xpath('.//div[@class="a-section a-spacing-small a-spacing-top-small"]//a/@href'):
                           # message('PRICE: 3')
                            price_link = loc_dom.xpath('.//div[@class="a-section a-spacing-small a-spacing-top-small"]//a/@href')
                            #message('PRICE LINK {0}'.format(price_link))
                            mydriver.get('https://www.amazon.com' + price_link[0])
                            #print(price_link)
                            # time.sleep(2)
                            minimal = 9999999
                            while True:
                                price_dom = mydriver.page()
                                curr_prices = price_dom.xpath('.//div[@class="a-row a-spacing-mini olpOffer" and contains(.,"New") and not(contains(.,"Used"))]//span[@class="a-size-large a-color-price olpOfferPrice a-text-bold"]/text()')
                               # print(curr_prices)
                                if curr_prices:
                                   # message('PRICE: 4')
                                    curr_prices = [float(re.sub(',','',pr.strip()[1:])) for pr in curr_prices]
                                   # print(curr_prices)
                                    new_min = min(curr_prices)
                                    if new_min < minimal:
                                        minimal = new_min
                                if price_dom.xpath('.//li[@class="a-last"]//a'):
                                    next_link = price_dom.xpath('.//li[@class="a-last"]//a/@href')
                                    mydriver.get('https://www.amazon.com'+ next_link[0])
                                    # time.sleep(2)
                                else:
                                    #message('На этой странице нет ни нужных цен ни переключателя на следующую страницу.')
                                    #print(minimal)
                                    #time.sleep(10000)
                                    break
                            if minimal != 9999999:
                                #message('PRICE: 5')
                                special = True
                                price = minimal
                            else:
                                #message('PRICE: 6')
                                price = 'N/A'
                        else:
                            #message('PRICE: 7')
                            price = 'N/A'

                if price == 'N/A' and special is False:
                    price = loc_dom.xpath('.//td[@class="comparison_baseitem_column" and contains(.,"$")]/span[@class="a-color-price a-text-bold"]/text()')
                    if bool(price) is True:
                        price = ''.join(price)
                        price = price.strip()
                        #print(price)
                        price = price[price.index("$"):]
                    else:
                        #message('PRICE: 8')
                        price = 'N/A'
                message('Price: {0}'.format(price))
                if price == 'N/A':
                    #ge('PRICE N/A')
                    total_counter += 1
                    continue
                condition = 'N/A'
                message('Condition: {0}'.format(condition))

                with_model_num += 1 # This is a counter for statistical information after the finishing of the script.
                total = [title, brand, colors, model_num, rating, platform, sizes, image, price, category, condition, curr_link]
                cursor.writerow(total)
                db_file.flush()
                message('Products with Item model number found in total: {0}'.format(with_model_num))
                total_counter +=1
            except Exception as e:
                message('!!!!!!!!!!!!!!!!!!!!!!!! A non-critical error occured!!!!!!!!!!!!!!!!!')
                #message('Сообщение ошибки: {0}, ссылка: {1}'.format(e,curr_link))
                traceback.print_exc()
                message('Код ошибки закончен')
        curr_page = +1
        page_counter +=1
        if not dom.xpath('.//li[@class="a-last"]') and page_counter > 1 and not dom.xpath('.//span[@id="pagnNextString"]'):
            message('We have scraped all the pages for the category: {0}. Finishing scraping for it.'.format(categ))
            return 0


# Open the file for writing data in it.
db_file = open(WRITE_DB, 'w', newline="", encoding='utf-8')
# db_file.write('\ufeff') # Use it to read UTF-8 files correctly then.
cursor = csv.writer(db_file, delimiter=';')
cursor.writerow(DB_HEADER)

# Load user-agents
message('Opening the file with headers...')
read_file = open(HEADER_DB,'r')
headers_list = list(read_file)
for idx in range(len(headers_list)):
    if '\n' in headers_list[idx]:
        headers_list[idx] = re.sub('\n', '', headers_list[idx])
read_file.close()
message('File read successfully. Read {0} headers. Go on...'.format(len(headers_list)))


message('Threads: {0}'.format(THREADS))
total_counter = 1
# Created a pool. One worker is one category.
pool = Pool(THREADS)
with_model_num = 0 # Just a counter.
curr_page = 0  # Just a counter for pagination.

# Start worker function. We will process as many categories at the same time as many THREADS we have.
tor()
pool.map(worker,list(MAIN_LINKS.keys()))

# Close pool and connection with the writing file.
pool.close()
pool.join()
db_file.close()

time_period = (time.time()-start_time)/60
message('Program finished successfully. Duration: {0} минут'.format(round(time_period,2)))
len_min = round(time_period/total_counter,5)
message('Duration per product: {} minutes или {} seconds'.format(len_min,len_min*60))
